//
//  UITextField_InputView.swift
//  Tarea02
//
//  Created by David Gregori Sánchez Rafael on 31/01/21.
//

import UIKit
extension UITextField {
    
    
//    1. Crear un objeto UIDatePicker
//    2. Setear datePickerMode a fecha
//   2.1 Agregar ka validación de fecha mínima al día actual
//    3. Asignar el datePicker como inputView
//    4. Crear un objeto UIToolbar
//    5. Crear UIBarButtonItem de tipo flexibleSpace para llenar el vacío
//    6. Crear un UIBarButtonItem de cancelación y agregar tapCancel como acción
//    7. Crear un UIBarButtonItem Done y pasar el target y el selector que recibimos como parámetro en la función
//    8. Setear los items al toolbar
//    9. Asiganr el toolbar como inputAccessoryView
//    
 
    func setInputViewDatePicker(target: Any, selector: Selector) {
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        datePicker.minimumDate = Date() //2.1
        // iOS 14 and above
        if #available(iOS 14, *) {// Added condition for iOS 14
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}
