//
//  ImagenPelicula.swift
//  Tarea02
//
//  Created by David Gregori Sánchez Rafael on 4/02/21.
//

import Foundation

class ImagenPelicula{
    
    class  func nombreGenero()->[String]{
           return [
            "touch",
            "art",
            "comedy",
            "fiction",
            "musical",
            "romantic",
            "sport",
            "terror"
           ]
    }
    
}
