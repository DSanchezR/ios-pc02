//
//  AgregarPeliculaViewController.swift
//  Tarea02
//
//  Created by David Gregori Sánchez Rafael on 30/01/21.
//

import UIKit

class AgregarPeliculaViewController: UIViewController {

    @IBOutlet weak var txtNombrePelicula: UITextField!
    @IBOutlet weak var txtDescripcion: UITextView!
    @IBOutlet weak var txtFechaEstreno: UITextField!
    @IBOutlet weak var txtImagenPelicula: UITextField!
    @IBOutlet weak var pickerViewImagenes: UIPickerView!
    
    
    var indexArraySelected: Int = 0
    let arrayImagen = ImagenPelicula.nombreGenero()
    
//    1. En la función viewDidLoad, se llama a la función extendida del UITextField: setInputViewDatePicker, con los parámetros de target y selector.
//    2. Esta función se llamará cuando toque el botón Listo sobre la barra de herramientas
//    2-1. Configurando el UIDatePicker desde el inputView del UITextField
//    2-2. Crear un DateFormatter
//    2-3. Setear el dateStyle o formato de fecha medium
//    2-4. Obtener la fecha en formato String y setearlo en el UITextField
//    2-5. Salir del UITextField
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Aplicar color del placeholder a UITextView
        txtDescripcion.text = "ingrese descripción"
        txtDescripcion.textColor = UIColor.officialApplePlaceholderGray
        
        // Aplicar borde de línea a UITextView
        txtDescripcion.layer.borderWidth = 1
        txtDescripcion.layer.borderColor = UIColor.black.cgColor

        self.txtFechaEstreno.setInputViewDatePicker(target: self, selector: #selector(tapDone)) //1
      
    }
    
    //2
    @objc func tapDone() {
        if let datePicker = self.txtFechaEstreno.inputView as? UIDatePicker { // 2-1
            let dateformatter = DateFormatter() // 2-2
            dateformatter.dateStyle = .medium // 2-3
            self.txtFechaEstreno.text = dateformatter.string(from: datePicker.date) //2-4
        }
        self.txtFechaEstreno.resignFirstResponder() // 2-5
    }
    
    @IBAction func grabarDatos(_ sender: Any) {
        
        
        if validarDatos() {
            // Grabar Objeto
            guard let nombrePelicula = self.txtNombrePelicula.text else {return}
            guard let descripcionPelicula = self.txtDescripcion.text else {return}
            guard let fechaEstrenoPelicula = self.txtFechaEstreno.text else {return}
            
            let objNuevaPelicula = ["nombre_pelicula":nombrePelicula, "descripcion_pelicula":descripcionPelicula, "fecha_estreno_pelicula":fechaEstrenoPelicula, "imagen_pelicula":arrayImagen[indexArraySelected]]
            
            // Usando clase Singleton
            DatosPeliculas.shared.arrayPeliculas.append(objNuevaPelicula)
            dismiss(animated: true) {
            }
        }
        else{
            let alert = UIAlertController(title: "Agregar Películas", message: "Debe de ingresar todos los datos solicitados.", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

            self.present(alert, animated: true)
        }

    }
    
    func validarDatos() -> Bool{
    
        if txtNombrePelicula.text == "" {return false}
        if txtDescripcion.text == "" {return false}
        if txtFechaEstreno.text == "" {return false}
        if indexArraySelected == 0 {return false}
        
        return true
    }
    
    
    @IBAction func salir(_ sender: Any) {
        dismiss(animated: true) {
        }
    }
    
}

extension AgregarPeliculaViewController: UITextViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    func textViewDidBeginEditing(_ textView: UITextView) {
        if txtDescripcion.textColor == UIColor.officialApplePlaceholderGray {
            txtDescripcion.text = nil
            txtDescripcion.textColor = UIColor.black
            //txtDescripcion.becomeFirstResponder()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtDescripcion.text == ""{
            txtDescripcion.text = "ingrese descripción"
            txtDescripcion.textColor = UIColor.officialApplePlaceholderGray
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayImagen.count
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 90
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var myImageView = UIImageView()
        
        myImageView = UIImageView(image: UIImage(named:arrayImagen[row]))
        
        return myImageView
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        txtImagenPelicula.text = row == 0 ? "Seleccione una imagen" : arrayImagen[row]
        indexArraySelected = row
    }

}

extension UIColor{

    static var officialApplePlaceholderGray: UIColor {
            return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
        }
    
}
