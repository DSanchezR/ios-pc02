//
//  DetallePeliculaViewController.swift
//  Tarea02
//
//  Created by David Gregori Sánchez Rafael on 30/01/21.
//

import UIKit

class DetallePeliculaViewController: UIViewController {
    
    var paramReceptor:[String:String]?
    
    @IBOutlet weak var txtTitulo: UILabel!
    @IBOutlet weak var txtFechaEstreno: UILabel!
    @IBOutlet weak var txtDescripcion: UITextView!
    @IBOutlet weak var imageViewGenero: UIImageView!
    @IBOutlet weak var lblGenero: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        guard  let paramDatos = paramReceptor else {return}
        
        let titulo = paramDatos["nombre_pelicula"] ?? ""
        let descripcion =  paramDatos["descripcion_pelicula"] ?? ""
        let fechaEstreno = paramDatos["fecha_estreno_pelicula"] ?? ""
        let generoPelicula = paramDatos["imagen_pelicula"] ?? "avatar"
        
        self.txtTitulo.text = titulo
        self.txtDescripcion.text = descripcion
        self.txtFechaEstreno.text = fechaEstreno
        self.imageViewGenero.image = UIImage(named: generoPelicula)
        self.lblGenero.text = generoPelicula
        
    }
}
