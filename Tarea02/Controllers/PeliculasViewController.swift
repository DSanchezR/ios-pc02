//
//  ViewController.swift
//  Tarea02
//
//  Created by David Gregori Sánchez Rafael on 30/01/21.
//

import UIKit

class PeliculasViewController: UIViewController{
    
    @IBOutlet weak var tvListadoPeliculas: UITableView!
    
    
    var arrayPeliculas = [[String:String]]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        arrayPeliculas = DatosPeliculas.shared.arrayPeliculas
        tvListadoPeliculas.reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "gotoDetalle" {
            guard  let indexSelected =  tvListadoPeliculas.indexPathForSelectedRow else {return}
            print("indexSelected:",indexSelected.row)
            let objDetalleDestino = segue.destination as? DetallePeliculaViewController
            objDetalleDestino?.paramReceptor = arrayPeliculas[indexSelected.row]
        }
        
    }

}
extension PeliculasViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPeliculas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CeldaPelicula", for: indexPath)
        let fila = indexPath.row
        let nombrePelicula = arrayPeliculas[fila]["nombre_pelicula"]
        let fechaEstrenoPelicula = arrayPeliculas[fila]["fecha_estreno_pelicula"]
        
        cell.textLabel?.text = nombrePelicula
        cell.detailTextLabel?.text = fechaEstrenoPelicula
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Listado de Películas"
    }
    
}

